# Reading TOMCAT DMP files with ImageJ/Fiji

Drop both the file DMP_Reader.class and the file DMP_Reader.java into the plugins directory (or any subfolder therein).
Make sure you also drop the file HandleExtraFileTypes.class to the plugins directory (and only there!) to enable the reader in the 'open' dialog.

If the reader does not work as expected (i.e. you cannot open DMP files) an additional step has to be taken.
This is due to changes in ImageJ/Fiji somewhere between versions 1.45p and 1.46k.
If you're lucky, this should not be necessary for Fiji versions > 1.48v.

You have to register the HandleExtraFileTypes.class plugin with the following command in your preferred terminal application.

> cd location/of/Fiji/plugins
> jar -uf IO_-2.1.0.jar HandleExtraFileTypes.class
