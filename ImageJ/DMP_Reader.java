import ij.plugin.*;
import java.awt.*;
import java.io.*;
import ij.*;
import ij.io.*;
import ij.process.*;
import ij.measure.*;

/**	Opens and displays DMP images.                                  */
/** Adapted from a plugin called PDS_Reader.java from Mike Martin.  */
/**     21-SEP-2010  bernd.pinzer@psi.ch                            */
public class DMP_Reader extends ImagePlus implements PlugIn {

	private static final String TITLE = "DMP Reader";
	private String directory, fileName;
	private DataInputStream f;
	private StringBuffer info = new StringBuffer(512);
	private double bscale, bzero;
	String keyword, value, line = "", sampleType, encodingType="",mapScale="";
	private int recordBytes,bitsPerPixel;

	public void run(String arg) {
                String path = getPath(arg);
                int islash = path.lastIndexOf('/');  
                directory = path.substring(0, islash+1);  
                fileName = path.substring(islash+1);  
		if (fileName==null)
			return;
		IJ.showStatus("Opening: " + directory + fileName);
		FileInfo fi = null;
		try {
			fi = getInfo();
		} catch (IOException e) {
			IJ.showMessage(TITLE, ""+e);
			return;
		}
		if (fi!=null && fi.width>0 && fi.height>0) {
			FileOpener fo = new FileOpener(fi);
			ImagePlus imp = fo.open(false);
			ImageProcessor ip = imp.getProcessor();
			setProcessor(fileName, ip);
			if (arg.equals("")) show();
		} //else 
			//IJ.error("This does not appear to be a PDS file.");
		IJ.showStatus("");
	}





	
	FileInfo getInfo() throws IOException {
		FileInfo fi = new FileInfo();
		fi.fileName = fileName;
		fi.directory = directory;
		fi.width = 0;
		fi.height = 0;
		fi.offset = 6;
		
		//BufferedReader f = new BufferedReader(new FileReader(directory+fileName));
                InputStream is = new FileInputStream(directory+fileName);
                byte[] buf = new byte[6];  // we're about to read the first 6 bytes
                is.read(buf, 0, 6);
                //one reason I hate java: there's no such thing as a uint16 data
                //type. To work around this, we have to do some gymnastics:
                int firstByte = 0;
                int secondByte = 0;
                firstByte  = (0x000000FF & ((int)buf[0]));
                secondByte = (0x000000FF & ((int)buf[1]));
                fi.width = (secondByte << 8 | firstByte);
                // because I never could remember that even a single day, here
                // are some explanations: 
                // first we are promoting a signed byte to an int, which
                // results in bits 8 through 31 set to 1 if the number happens
                // to be larger than 127. the bitwise and with 0x000000FF wipes
                // out all but the first 8 bits.
                // The last line deals with endianness: the binary file is
                // little endian, while java is in general big endian.
                //System.out.println(fi.width);
		
                firstByte  = (0x000000FF & ((int)buf[2]));
                secondByte = (0x000000FF & ((int)buf[3]));
                fi.height = (secondByte << 8 | firstByte);
                //System.out.println(fi.height);

                fi.fileType = FileInfo.GRAY32_FLOAT;
                fi.intelByteOrder = true;

    		fi.valueUnit = "gray value";
		return fi;
	} // setting the FileInfo  



        private String getPath(String arg) {  
          if (null != arg) {  
            if (new File(arg).exists()) return arg;  
          }  
          // else, ask:  
          OpenDialog od = new OpenDialog("Choose a .DMP file", null);  
          String dir = od.getDirectory();  
          if (null == dir) return null; // dialog was canceled          
          dir = dir.replace('\\', '/'); // Windows safe  
          if (!dir.endsWith("/")) dir += "/";  
          return dir + od.getFileName();  
        }  

} // end of DMP reader class
