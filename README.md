# Reading TOMCAT DMP files

This repository pools code to read DMP files from the [TOMCAT](http://psi.ch/sls/tomcat) beamline.

The DMP file format is the simplest imaginable file format for storing floating point data: it consists of 3 unsigned 16bit integers specifying the image dimensions, followed by a stream of 32-bit floats in [little-endian byte order](http://geekandpoke.typepad.com/geekandpoke/2011/09/simply-explained-1.html). 

Each [subdirectory](https://git.psi.ch/tomcat/TOMCAT-DMP-Files/tree/master) contains the necessary files for reading DMP files with the software like the directory name.
One example DMP file is present as [example.DMP](example.DMP) and is referenced from each code sample.

Pull requests to add further tools and [issue reports](https://git.psi.ch/tomcat/TOMCAT-DMP-Files/issues) for problems are welcome!
