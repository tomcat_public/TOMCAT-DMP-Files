pro write_dmp,path,image
 
 ; function for writing the images .DMP from TOMCAT 32 Bit
 ;
 
 ; Notes:  The DMP file format is the simplest imaginable file format 
 ;         for storing floating point data: it consists of 3 unsigned 16bit
 ;         integers N_i specifying the image dimensions (yes, it is possible
 ;         to store 3d data, but we only use it for slices, i.e. the third number is 1),
 ;         followed by a stream of \prod_{i} N_i 32-bit floats in little-endian byte order. 
 
 ; by Alberto Astolfo 24-07-2013
 
 ;-----------------------------------
    
 ; reading the image size
 image2=image
 s_ize=size(image2)
 
 s1=s_ize(1)
 s2=s_ize(2)
 
 
 OPENW,1, path

 header=uint([s1,s2,0])
 writeu,1,header
 writeu,1,image2
 close,1

end