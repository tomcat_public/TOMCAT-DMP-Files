function read_dmp,path
 
 ; function for reading the images .DMP from TOMCAT 32 Bit
 ;
 
 ; Notes:  The DMP file format is the simplest imaginable file format 
 ;         for storing floating point data: it consists of 3 unsigned 16bit
 ;         integers N_i specifying the image dimensions (yes, it is possible
 ;         to store 3d data, but we only use it for slices, i.e. the third number is 1),
 ;         followed by a stream of \prod_{i} N_i 32-bit floats in little-endian byte order. 
 
 ; by Alberto Astolfo 11-02-2013
 
 ;-----------------------------------
    
 ; open the file containing the image
 
 OPENU,1, path
 ;Get file status to get the file size
 status = FSTAT(1)
 header=intarr(3)
 readu,1,header
 vett=fltarr((status.size-6)/4) ; vector that will have the data with no header
 readu,1,vett
 close,1

 s1=float(header(0)) ; get the x size
 s2=float(header(1)) ; get the y size
 
image=fltarr(s1,s2)  ; prepare the output image

for i=0,s2-1 do image(*,i)=vett(i*s1:(i+1)*s1-1)

return,image

end